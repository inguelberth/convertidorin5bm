package org.inguelberth.sistema;
import org.inguelberth.utilidades.Calculadora;
/**
	Clase principal, que inicia la aplicacion
*/
public class Principal{
	public static void main(String args[]){
		if(args.length>0){
			int valorDevuelto = Calculadora.convertidorBinDec(args[0]);
			System.out.println("		Resultado: "+valorDevuelto);
		}
	}
}
