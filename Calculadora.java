package org.inguelberth.utilidades;
/**
	Esta clase me sirve como conversor
	@author Inguelberth Garcia
*/
public class Calculadora{
/**
	Este metodo convierte de decimal a binario
	@param num Numero decimal a convertir
	@return El numero binario convertido
*/
	public static int convertidorBinDec(String num){
		int resultado=0;
		int posicion=1;
		for(int contador=num.length()-1;contador>=0;contador--){
			int n =Integer.parseInt(num.substring(contador, contador+1));
			resultado=resultado+(posicion*n);
			posicion*=2;			
		}
		return resultado;
	}
}
